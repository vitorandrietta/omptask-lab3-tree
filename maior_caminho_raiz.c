#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

/*improvement guide
 *
 * 1 -> group all leafs by father
 *
 * 2 -> for each of these groups call a task that chooses the leaf with the greater value
 * and call a task summing this number using the father leaf as parameters
 *
 *
 *
 * */




typedef struct st_list {
    struct st_node *element;
    struct st_list *next;
} list_node;

typedef struct st_node {
    long int id;
    long int parent;
    int key;
    list_node *children;
} node;


typedef struct st_queue {
    node **array;
    long int head;
    long int tail;
    long int n;
} queue;

queue *init_queue(long int n) {
    queue *q = (queue *) malloc(sizeof(queue));
    q->head = 0;
    q->tail = 0;
    q->n = n;
    q->array = (node **) malloc(sizeof(node *) * n);
    return q;
}

void process_path_size(node*father, long int * path, int nthreads){
    if (father != NULL){
        int current_threads = omp_get_num_threads();
        #pragma omp parallel default(none) shared(path,current_threads,nthreads)  firstprivate(father)
        {
            #pragma omp single nowait
            {
                list_node* current = father->children;
                if(current){
                    while(current = current->next){
                    #pragma omp task  default(none) depend(in: path[father->id]) depend(out: path[current->element->id]) \
                                firstprivate(father, current) shared(path,nthreads,current_threads) \
                                final (current_threads > nthreads) mergeable
                                {   path[current->element->id] = path[current->element->parent] + current->element->key;
                                    process_path_size(current->element, path, nthreads);
                                }
                    }

                }


            }

        }


    }



}

void enqueue(queue *q, node *x) {
    (q->array)[(q->tail)++] = x;
    if (q->tail == q->n)
        q->tail = 0;
}

node *dequeue(queue *q) {
    if (q->head == q->tail)
        return NULL;
    node *ret = (q->array)[(q->head)++];
    if (q->head == q->n)
        q->head = 0;
    return ret;
}

void print_node(node *x) {
    printf("node:%ld key:%d\n", x->id, x->key);
    printf("parent: %ld\nchildren:", x->parent);
    list_node *current = x->children;
    while (current) {
        printf("%ld ", (current->element)->id);
        current = current->next;
    }
    printf("\n");
}

void print_map(node **map, long int n) {
    for (long int i = 0; i < n; i++) {
        if (map[i])
            print_node(map[i]);
    }
}

void add_child(node *parent, node *child) {

    list_node *head = parent->children;
    list_node * new = (list_node *) malloc(sizeof(list_node));
    new->element = child;
    new->next = head;
    parent->children = new;
}


int main() {

/* Part 1: Input and building the tree */

    long int n;
    scanf("%ld", &n);

    if (!n) {
        printf("0\n");
        return 0;
    }

    // allocating a map to store the nodes
    node **map = (node **) malloc(sizeof(node) * n);

    //begin by the leafs

    // reading and creating the nodes
    int key;
    for (long int i = 0; i < n; i++) {
        scanf("%d", &key);
        node *
        new = (node *) malloc(sizeof(node));
        new->id = i;
        new->key = key;
        new->parent = -1;
        new->children = NULL;

        map[i] = new;
    }

    // reading the children of each node
    long int id_child, id_parent;
    int n_child;
    for (long int i = 0; i < n; i++) {
        scanf("%ld", &id_parent);
        node *current = map[id_parent];
        node *child = NULL;

        scanf("%d", &n_child);
        for (int j = 0; j < n_child; j++) {
            scanf("%ld", &id_child);
            child = map[id_child];
            child->parent = id_parent;
            add_child(current, child);
        }


    }
    node *root, *current;
    for (long int i = 0; i < n; i++) {
        current = map[i];
        if (current && current->parent == -1) {
            root = current;
            break;
        }
    }

//    printf("valor root:%d\n",root->key);
    double t_start = omp_get_wtime();


    long int *path = (long int *) malloc(sizeof(long int) * n);
    path[root->id] = root->key;

    #pragma omp parallel
    {
        process_path_size(root,path,omp_get_num_procs());
        #pragma omp taskwait

    };


    double t_end = omp_get_wtime();
    long int longest_path = path[0];

#pragma omp parallel for reduction(max:longest_path)
    for(long int i = 1;i < n; i++) {
        longest_path = longest_path > path[i] ? longest_path : path[i];
    }


    printf("%ld\n", longest_path);
    printf("%lf\n",t_end-t_start);

    for(int i=0;i<n;++i){
        free(map[i]->children);
        free(map[i]);
    }
    free(path);
    free(map);
    return 0;
}
